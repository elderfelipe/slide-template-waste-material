---
title: Please change this with the title of your presentation
description: Please change this with a one-line description of your presentation
author: Please replace this with your name(s)
keywords: marp,marp-cli,slide
url: Please replace this with the URL of the deployed presentation
marp: true
image: Please replace this with a URL of an avatar/icon of your presentation
---

# My Validated Concept
## It will crush the world

---

<style>
img[alt~="center"], .center, .fence {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.fence img {
    width: auto;
    max-width: 100%;
    height: auto;
    max-height: 100%;
}
.margin-top {
    margin-top: 1em;
}
.padding-top {
    padding-top: 1em;
}
</style>

# Checklists

These are always helpful
- [x] _==&leftarrow; A highlighted, right-floating segment==_{.float-right}Done
- [x] _==&leftarrow; A highlighted, right-floating segment==_{.float-right}Done
- [ ] _==&leftarrow; A highlighted, right-floating segment==_{.float-right}Still to be done
- [ ] _==&leftarrow; A highlighted, right-floating segment==_{.float-right}Still to be done

---

# Regular slides are okay

> Quoted blocks are fancy, they add context

Paragraphs get to the point :) (check emoji textual representations [here](https://github.com/markdown-it/markdown-it-emoji/blob/master/lib/data/shortcuts.js))

---

# Mindmaps

But also, any **plantuml** diagram...

::: fence
@startuml

@startmindmap
* Big idea
** Smaller idea
*** Even smaller idea
*** ...
*** ...
** ...
** ...
@endmindmap

@enduml
:::
